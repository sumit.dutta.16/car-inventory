module.exports = ( inventory , id) =>{
    if(inventory === undefined || inventory.length === 0 || id === undefined){
        return [];
    }else{
        for(i = 0; i < inventory.length; i++){
            if(inventory[i].id === id ){
                return inventory[i];
            }
        }
    }   
}